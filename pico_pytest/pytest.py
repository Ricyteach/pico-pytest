import importlib.util
from pathlib import Path
import sys


def main():
    results = []
    for path in Path.cwd().glob("**/test_*.py"):
        spec = importlib.util.spec_from_file_location(path.stem, path)
        module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module)
        for name in [n for n in dir(module) if n.startswith("test_")]:
            obj = getattr(module, name)
            if callable(obj):
                try:
                    obj()
                    results.append((".", f"{module}::{name}"))
                except Exception as e:
                    results.append(("F", f"{module}::{name}"))
    print("=" * 80, "\n", "".join(r[0] for r in results), "\n", "=" * 80)
    if any(r[0] == "F" for r in results):
        sys.exit(
            f"{results.count('F')} tests failed :(\n"
            + "\n".join(r[1] for r in results if r[0] == "F")
        )
